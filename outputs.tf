output "vpc_id" {
  value       = data.aws_vpc.vpc.id
  description = "Match VPC"
}

output "subnet" {
  value       = {
    public    = local.subnet_ids_public_list
    private   = local.subnet_ids_private_list
  }
  description = "Match set of Subnet IDs"
}

output "security_groups" {
  value = {
    default       = data.aws_security_group.default.id
    load_balancer = data.aws_security_group.load_balancer.id
    ssh           = data.aws_security_group.ssh.id
  }
  description     = "Match set of security groups"
}
