variable "cidr_block" {
  type        = string
  description = "VPC cidr block"
}

variable "vpc_name" {
  type        = string
  description = "VPC Name"
}

variable "subnet" {
  type = object({
    public    = list(string)
    private   = list(string)
  })
  description = "Subnet locator descriptors"
}
