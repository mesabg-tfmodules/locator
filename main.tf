terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 4.0.0"
    }
  }
}

data "aws_vpc" "vpc" {
  cidr_block = var.cidr_block
  filter {
    name    = "tag:Name"
    values  = toset([var.vpc_name])
  }
}

data "aws_subnets" "public" {
  count     = length(var.subnet.public) != 0 ? 1 : 0

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name    = "tag:Name"
    values  = var.subnet.public
  }
}

data "aws_subnets" "private" {
  count     = length(var.subnet.private) != 0 ? 1 : 0

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name    = "tag:Name"
    values  = var.subnet.private
  }
}

locals {                                                            
  subnet_ids_public_string = length(var.subnet.public) != 0 ? join(",", data.aws_subnets.public[0].ids) : ""
  subnet_ids_public_list = length(var.subnet.public) != 0 ? split(",", local.subnet_ids_public_string) : []
  subnet_ids_private_string = length(var.subnet.private) != 0 ? join(",", data.aws_subnets.private[0].ids) : ""
  subnet_ids_private_list = length(var.subnet.private) != 0 ? split(",", local.subnet_ids_private_string) : []
}

data "aws_security_group" "default" {
  name    = "default"
  vpc_id  = data.aws_vpc.vpc.id
}

data "aws_security_group" "load_balancer" {
  name    = "load-balancer"
  vpc_id  = data.aws_vpc.vpc.id
}

data "aws_security_group" "ssh" {
  name    = "ssh"
  vpc_id  = data.aws_vpc.vpc.id
}
