# Locator Module

This module is capable to locate VPC and Route53 general resources
NOTE: Certificate MUST be always present on us-east-1 region.

Module Input Variables
----------------------

- `domain` - base domain
- `cidr_block` - vpc cidr block
- `vpc_name` - vpc name
- `subnet` - subnet locators

Usage
-----

```hcl
module "locator" {
  source      = "git::https://gitlab.com/mesabg-tfmodules/locator.git"

  domain      = "foo.com"
  cidr_block  = "10.0.0.0/16"
  vpc_name    = "Some Name"
  subnet      = {
    public    = ["Public_Subnet_A", "Public_Subnet_B"]
    private   = ["Private_Subnet_A", "Private_Subnet_B"]
  }
}
```

Outputs
=======

 - `zone_id` - Match Route53 zone_id
 - `certificate_arm` - Match ACM Certificate
 - `vpc_id` - Match VPC id
 - `subnet` - Match Subnets IDs
 - `security_groups` - Match Security Groups IDs

Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
